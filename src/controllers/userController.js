// Import sequelize
const { query } = require('express');
const sequelize = require('../models/index');
const initModels = require('../models/init-models');
const model = initModels(sequelize);

const getUser = async (req, res) => {
  // render all data (like GET in CRUD)

  try {
    let data = await model.user.findAll();
    res.status(200).send(data);
  } catch (error) {
    res.status(500).send("Lỗi backend");
  }
};

const getUserId = async (req, res) => {
  try {
    let { id } = req.params;
    // render selected data, here by user_id
    let dataFiltered = await model.user.findOne({
      where: {
        user_id: id,
      },
    });
    if (dataFiltered) {
      res.status(200).send(dataFiltered);
    } else {
      res.status(400).send("User không tồn tại");
    }
  } catch (error) {
    res.status(500).send("Lỗi backend");
  }
};

const createUser = async (req, res) => {
  try {
    let { full_name, email, pass_word } = req.body;

    // ES6 => object literal
    let model = {
      full_name,
      email,
      pass_word,
    };

    // thêm data vào DB
    let data = await model.user.create(model);
    if (data) {
      res.send("Thêm user thành công");
    }
  } catch (error) {
    res.status(500).send("Lỗi backend");
  }
};

const updateUser = async (req, res) => {
  try {
    let { id } = req.params;
    console.log("id==========", id);
    // render selected data, here by user_id
    let dataFiltered = await model.user.findOne({
      where: {
        user_id: id,
      },
    });
    if (dataFiltered) {
      // update user
      let { full_name, email, pass_word } = req.body;
      // ES6 => object literal
      let model = {
        full_name,
        email,
        pass_word,
      };
      // UPDATE user SET .... WHERE user_id = id;
      let dataUpdate = await model.user.update(model, {
        where: {
          user_id: id,
        },
      });
      if (dataUpdate[0] == 1) {
        res.status(200).send("Cập nhật user thành công");
      } else {
        res.status(400).send("Không có gì mới để cập nhật");
      }
    } else {
      res.status(400).send("User không tồn tại");
    }
  } catch (error) {
    res.status(500).send("Lỗi backend");
  }
};

const deleteUser = async (req, res) => {
  try {
    let { id } = req.params;
    let dataFiltered = await model.user.destroy({
      where: {
        user_id: id,
      },
    });
    if (dataFiltered) {
      res.status(200).send("Xóa user thành công");
    } else {
      res.status(400).send("User không tồn tại");
    }
  } catch (error) {
    res.status(500).send("Lỗi backend");
  }
};

module.exports = {
  getUser,
  getUserId,
  createUser,
  updateUser,
  deleteUser,
};
